@extends('layout')

@section('conteudo')

<h1> Alteração de pais</h1>

<form action="{{route('pais-alterar', [$pais->idpais])}}" method="POST">
    {{ csrf_field() }}
    
    <div class="col-md-5 col-sm-3">
    
    <div class="form-group">
    
        <input type="text" name="nome" class="form-control visible-lg" id="nome" placeholder="Nome" value="{{$pais->nome}}" />
    </div>
      
      <div class="form-group">
        <label for="sigla">Password</label>
        <input type="text" class="form-control" id="sigla" name="sigla" placeholder="Sigla" value="{{$pais->sigla}}">
      </div>
      
      <a class="btn btn-warning" href="{{  route('pais-lista') }}" role="button">Voltar</a>
      
      <button type="submit" class="btn btn-default">
          <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Cadastrar
      </button>
      
    
    </div>
    
    
</form>



@endsection

