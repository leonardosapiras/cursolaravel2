<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome2');
});

Route::get('pais/lista', 'PaisController@exibirPaises')->name('pais-lista')->middleware('auth');


Route::get('pais/cadastro', 'PaisController@exibirCadastro')->name('pais-cadastro');
Route::post('pais/cadastrar', 'PaisController@cadastrar')->name('pais-cadastrar');

Route::get('pais/alteracao/{idpais}', 'PaisController@exibirAlteracao')->name('pais-alteracao');
Route::post('pais/alterar/{idpais}', 'PaisController@alterar')->name('pais-alterar');
Auth::routes();

Route::get('/home', 'HomeController@index');
