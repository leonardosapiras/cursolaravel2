<?php

use Illuminate\Database\Seeder;

class CidadeTesteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estados = array(
			['idestado' => 4, 'nome' => 'X', 'sigla' => 'X', 'idpais' => 1],
			['idestado' => 5, 'nome' => 'Y', 'sigla' => 'y', 'idpais' => 2],

		);
		DB::table('estado')->insert($estados);
		$this->command->info('Cadastrando estados!');
		
		
		$cidades = array(
			['idestado' => 4, 'nome' => 'X1'],
			['idestado' => 4, 'nome' => 'X2'],

		);
		DB::table('cidade')->insert($cidades);
		$this->command->info('Cadastrando cidades!');
    }
}
