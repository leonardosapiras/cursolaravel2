<?php

use Illuminate\Database\Seeder;
use App\Pais;
use App\Estado;

class EstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pais = Pais::where('nome', '=', 'Brasil')->first();
        
        $estado = new Estado();
        $estado->idpais = $pais->idpais;
        $estado->nome = 'Rio Grande do Sul';
        $estado->sigla = 'RS';
        $estado->regiao = 'Sul';
        $estado->save();
        
        $estado = new Estado();
        $estado->idpais = $pais->idpais;
        $estado->nome = 'Paraná';
        $estado->sigla = 'PR';
        $estado->regiao = 'Sul';
        $estado->save();
		
		$this->command->info('Cadastrando estados!');
    }
}
